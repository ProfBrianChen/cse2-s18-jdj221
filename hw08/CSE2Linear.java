//James Johnson
//Homework 8 
////// CSE2Linear //////
import java.util.Scanner;//import scanner class
import java.util.Random; // import random class
public class CSE2Linear{ //class CSE2Linear
  
  //binary search
  public static void binary(int[] grade){
    Scanner myScanner = new Scanner(System.in);
    int high = 14; //high
    int low = 0; //low
    System.out.println("Input a number to search for: ");
    int input1 = myScanner.nextInt(); //takes input 
    int count = 1; //counts iterations
    while (low <= high){
      int middle = ((high + low) / 2);
      if (grade[middle] < input1){
        low = middle + 1;
      }
      else if (grade[middle] > input1){
        high = middle + 1;
      }
      else if (grade[middle] == input1){ //if found
        System.out.println("Your input of " + input1 + " was found in " + count + " iterations");
        break;
      }
      count++;
    }
    while (low > high){ //if not found
      System.out.println("Your input of " + input1 + " was not found in " + count + " iterations");
      break;
    }
    return; //return 
  }
  
  //Scramble Method
  public static void scramble(int[] grade){
    Scanner myScanner = new Scanner(System.in); //initializes scanner class
    Random randomGenerator = new Random(); //initializes random class
    for (int i = 0; i < grade.length; i ++){
      //finds random member to swap
      int target = (int)(grade.length * Math.random());
      //swaps
      int mix = grade[target];
      grade[target] = grade[i];
      grade[i] = mix;
    }
    int size = grade.length; //length of array
    System.out.println("The scrambled array looks like: ");
    //prints array out
    for (int j = 0; j < size; j++){
      int u = grade[j];
      System.out.print(" " + u);
    }
    System.out.println("");
    return; //return
  }
  
  //Linear Search Method
  public static void linear(int[] grade){
    Scanner myScanner = new Scanner(System.in); //initializes scanner class
    System.out.println("Input a number to search for: ");
    int input2 = myScanner.nextInt(); //takes scanner input
    int count2 = 0; //counts iteration
    int i;
    for (i =0; i < grade.length; i++){
      if (grade[i] == input2){ //if equal
        count2++; //increment count2
        System.out.println("Your input of " + input2 + " was found in " + count2 + " iterations");
      break;
      }
    count2++; //increment count2
    }
    if (i == grade.length){ //if not found
         System.out.println("Your input of " + input2 + " was not found in " + count2 + " iterations");
    }
    return; //return
  }
  
  public static void main(String[] arg){
  Scanner myScanner = new Scanner(System.in);
  int[] grade=new int[15]; //array size 15
  System.out.println("Enter 15 ascending integers between 0-100");
  //takes 15 ints as input and tests them
  //returns if any errors happen
  for (int i = 0; i < 15; i++){
    if(myScanner.hasNextInt()){ //if int
      grade[i] = myScanner.nextInt();
    }
    else if(!myScanner.hasNextInt()){ //if not int
      System.out.println("Error: Input not an integer.");
      return;
    }
    if((grade[i] > 100) || (grade[i] < 0)){ //if out of desired range
      System.out.println("Error: Input outside of desired range.");
      return;
      }
    if(i > 0 && (grade[i]) <= (grade[i-1])){ //if not ascending
      System.out.println("Error: Input not ascending.");
      return;
    }
  }
  int size = grade.length; //length of array
    System.out.println("The array looks like: ");
    //prints array
    for (int j = 0; j < size; j++){
      int u = grade[j];
      System.out.print(" " + u);
    }
  System.out.println("");
  
  binary(grade); //calls binary method
   
  scramble(grade); //calls scramble method
    
  linear(grade); //calls linear method
  System.out.println("Congrats you're done!!! Hopefully code anywhere doesn't delete this again.");
 }
}