import java.util.Scanner; //import scanner class
import java.util.Random; // import random class
public class RemoveElements{ //class remove elements
  
  public static int[] randomInput(){ //random input method
		//creates an array of size 10 with random integers between 0-10
	  int num2[]=new int[10];
		  for (int i = 0; i < 10; i++){ 
			  num2[i] = (int)(Math.random() * 10);
		  }
	 return num2; //returns num2
  }
	
	public static int[] delete(int list[], int index){ //delete method
	int[] list2 = new int[list.length - 1]; //new array of integers
	if (index >= 10 || index < 0 ){ //if out of bounds
		System.out.println("The index is not valid");
		list2 = list; //sets lists equal to each other
		return list2; //return
	}
    for (int i = 0; i < list.length - 1; i++){
	    if (i < index){ //if less than index
			list2[i] = list[i];
			}
			else { //else 
				list2[i] = list[i + 1];
			} 
	}
	return list2; //returns list2
}
	
	public static int[] remove(int list[],int target){ //remove method
		int count = 0; 
		//counts iterations in array
		for (int i = 0; i < 10; i++){
      if (list[i] == target){
        count++;
      }
    }
		System.out.println("Target shows up " + count + " times");
		int check = 0; //for list
		int index = 0; //for list 3
	int[] list3 = new int[list.length - count]; //sets new list length
	while (check < list.length && index < list3.length){ //while check and index are less than there respective lists
		if(list[check] != target){ //if not equal to target
			list3[index] = list[check]; //set positions equal
			//increment both
			check++; 
			index++;
		}
		
		else { //else 
			//use new int tock to test how many of the target value are in a row
			for(int tock = check; tock < list.length; tock++){ 
				if(list[tock] == target){ //if equal to target
					check++; //increment check to leave for loop
				} 
				else{ //else
					tock = list.length; //leave for loop
				}
			}	
		}
	}
		return list3; //return list 3
	}

//main method
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in); 
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
    System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
}
 
