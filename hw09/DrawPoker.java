//James Johnson
//Homework 9
////// Draw Poker //////
import java.util.Scanner;//import scanner class
import java.util.Random; // import random class
public class DrawPoker{ //class Draw Poker
  public static void shuffle(int[] deck){
    Scanner myScanner = new Scanner(System.in); //initializes scanner class
    Random randomGenerator = new Random(); //initializes random class
    for (int i = 0; i < deck.length; i ++){
      //finds random member to swap
      int target = (int)(deck.length * Math.random());
      //swaps
      int mix = deck[target];
      deck[target] = deck[i];
      deck[i] = mix;
    }
    int size = deck.length; //length of array
    return; //return
  }
  
  public static int[] deal(int[] hand, int player){
    int[] hand1 = new int[5]; //new array of length 5  
    int count = player; //count = player
    for (int i = 0; i < 5; i++){ //sets new array values for each hand
      hand1[i] = (hand[count]);
      count += 2;
    }
    return hand1; //return new array
  }
  
  public static boolean flush(int[] hand){
    int diamonds = 0;
    int clubs = 0;
    int hearts = 0;
    int spades = 0;
    boolean flag = false;
    for (int i = 0; i < 5; i++){
      if (hand[i] > 0 && hand[i] <=13){
        diamonds++;
    }
      else if (hand[i] > 13 && hand[i] <=26){
        clubs++;
    }
      else if (hand[i] > 26 && hand[i]<=39){
        hearts++;
    }
      else if (hand[i] > 39 && hand[i] <=52){
        spades++;
    } 
    }
    if(diamonds == 5){
      flag = true;
    }
    else if(clubs == 5){
      flag = true;
    }
    else if(hearts == 5){
      flag = true;
    }
    else if(spades == 5){
      flag = true;
    }
  return flag;
  }
  public static boolean pair(int[] hand){
    boolean flag = false;
    for (int i = 0; i < 5; i++){
      hand[i] = (hand[i]%13);
    }
    for (int i = 0; i < 5; i++){
      for (int j = (i+1); j < 5;j++){
        if (hand[i] == hand[j]){
          flag = true;
        break;
        }
      }
    }
    return flag;
  }
  
  public static boolean threeKind(int[] hand){
    boolean flag = false;
    for (int i = 0; i < 5; i++){
      hand[i] = (hand[i]%13);
    }
    for (int i = 0; i < 5; i++){
      for (int j = (i+1); j < 5;j++){
        for (int k = (j+1); k < 5; k++){
          if (hand[i] == hand[j] && hand[i] == hand[k]){
            flag = true;
          break;
        }
      }
    }
  }
  return flag;
  }
  
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    int randomCard = (int)((Math.random() * 51) + 1); //random card
    String suit = ""; //initializes string
    int cardValue = (randomCard % 13); //initialized card value
    String cardName = "";
    //////// Assignes suit based on card number ////////
    if (randomCard > 0 && randomCard <=13){
      suit = "diamonds";
    }
    else if (randomCard > 13 && randomCard <=26){
      suit = "clubs";
    }
    else if (randomCard > 26 && randomCard <=39){
      suit = "hearts";
    }
    else if (randomCard > 39 && randomCard <=52){
      suit = "spades";
    }
//     System.out.println(randomCard + " : " + suit); //prints card number and suit
//     System.out.println(cardValue); //prints card value
//     ////// assigns card name //////
    switch(cardValue){
      case 1:
        cardName = "Ace";
      break;
      case 2:
        cardName = "Two";
      break;
      case 3:
        cardName = "Three";
      break;
      case 4:
        cardName = "Four";
      break;
      case 5:
        cardName = "Five";
      break;
      case 6:
        cardName = "Six";
      break;
       case 7:
        cardName = "Seven";
      break;
      case 8:
        cardName = "Eight";
      break;
      case 9:
        cardName = "Nine";
      break;
      case 10:
        cardName = "Ten";
      break;
      case 11:
        cardName = "Jack";
      break;
      case 12:
        cardName = "Queen";
      break;
      case 0:
        cardName = "King";
      break;
    }
   // System.out.println(cardName); //prints card name
    
    int[] deck = new int[52]; //Declaration
    for (int i = 0; i < 52; i++){
      deck[i] = (i + 1); 
      //System.out.print(deck[i]);
    }
    shuffle(deck); //calls shuffle method
    int[] hand1 = deal(deck, 0); //calls deal method and assigns to hand1
    System.out.println("hand1: ");
    for (int j = 0; j < 5; j++){
      int u = (hand1[j]%13);
      System.out.print(" " + u);
    }
    System.out.println("");
    System.out.println("hand2: ");
    int[] hand2 = deal(deck, 1); //calls deal method and sets equal to hand2
    for (int j = 0; j < 5; j++){
      int u = (hand2[j]%13);
      System.out.print(" " + u);
    }
    System.out.println("");
    
    //////Tests for Flush//////
    boolean flag1 = flush(hand1);
    if (flag1 == true){
      System.out.println("Hand1 got a flush");
    }
    boolean flag2 = flush(hand2);
    if (flag2 == true){
      System.out.println("Hand2 got a flush");
    }
    
    //////Tests for 3 of a kind
    Boolean threeKind1 = threeKind(hand1);
    int value3 = -1;
    if (threeKind1 == true){
      System.out.println("Hand1 got a three of a kind!");
    }
    for (int i = 0; i < 5; i++){
      for (int j = (i+1); j < 5;j++){
        for (int k = (j+1); k < 5; k++){
          if (hand1[i] == hand1[j] && hand1[i] == hand1[k]){
            value3 = hand1[i];
          break;
        }
      }
    }
  }
    
    Boolean threeKind2 = threeKind(hand2);
    int value4 = -1;
    if (threeKind2 == true){
      System.out.println("Hand2 got a three of a kind!");
    }
    for (int i = 0; i < 5; i++){
      for (int j = (i+1); j < 5;j++){
        for (int k = (j+1); k < 5; k++){
          if (hand2[i] == hand2[j] && hand2[i] == hand2[k]){
            value4 = hand2[i];
          break;
        }
      }
    }
  }
    ////// tests for 2 of kind //////
    Boolean pair1 = pair(hand1);
    int value1 = -1;
    for (int i = 0; i < 5; i++){
      for (int j = (i+1); j < 5;j++){
        if (hand1[i] == hand1[j]){
          value1 = hand1[i];
        break;
        }
      }
    }
    if (pair1 == true){
      if (value1 == value3){
        if (value1 != -1){
      System.out.println("Hand1 got a pair!");
        }
      }
    }
    
    Boolean pair2 = pair(hand2);
    int value2 = -1;
    for (int i = 0; i < 5; i++){
      for (int j = (i+1); j < 5;j++){
        if (hand2[i] == hand2[j]){
          value2 = hand2[i];
        break;
        }
      }
    }
    if (pair2 == true){
      if (value2 == value4){
        if (value2 != -1){
      System.out.println("Hand1 got a pair!");
        }
      }
    }
    
    
    //////Test full house//////
    boolean fullhouse1 = false;
    if (pair1 == true && threeKind1 == true){
      if (value1 != value3){
          fullhouse1 = true;
          System.out.println("Hand1 got a full house");
      }
    }
    boolean fullhouse2 = false;
    if (pair2 == true && threeKind2 == true){
      if (value2 != value4){
        fullhouse2 = true;
        System.out.println("Hand2 got a full house");
      }
    }
    
    //////Test Scores//////
    int score1 = 0;
    int score2 = 0;
    //Pair
    if (pair1 == true){
      score1 = 2;
    }
    if (pair2 == true){
      score2 = 2;
    }
    //Three of a kind
    if (threeKind1 == true){
      score1 = 3;
    }
    if (threeKind2 == true){
      score2 = 3;
    }
    //Flush
    if (flag1 == true){
      score1 = 4;
    }
    if (flag2 == true){
      score2 = 4;
    }
    //Full house
    if (fullhouse1 == true){
      score1 = 5;
    }
    if (fullhouse2 == true){
      score2 = 5;
    }
    System.out.println("Score 1: " + score1);
    System.out.println("Score 2: " + score2);
    //Who wins?
    if (score1 > score2){
      System.out.println("Hand1 wins!");
    }
    if (score2 > score1){
      System.out.println("Hand2 wins!");
    }
    if (score1 == score2){
      int high1 = 0;
      int high2 = 0;
      for (int j = 0; j < 5; j++){
        hand1[j] = hand1[j] % 13;
        for (int i = j+1; i < 5; i++){
          if (hand1[i] >= hand1[j]){
            high1 = hand1[i];
          }
        }
      }
      System.out.println("Hand1 has a high card of: " + high1);
      for (int j = 0; j < 5; j++){
        hand2[j] = hand2[j] % 13;
        for (int i = j+1; i < 5; i++){
          if (hand2[i] >= hand2[j]){
            high2 = hand2[i];
          }
        }
      }
      System.out.println("Hand2 has a high card of: " + high2);
      if (high1 > high2){
        System.out.println("Hand1 wins!");
      }
      if (high1 < high2){
        System.out.println("Hand2 wins!");
      }
      if (high1 == high2){
        System.out.println("Its a tie!");
      }
    }
  }
}