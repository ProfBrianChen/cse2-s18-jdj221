import java.util.Scanner;
public class Hw05 {

	public static void main(String[] args) {
		//Main Method
    Scanner myScanner = new Scanner( System.in );
    boolean Flag = true; //Sets boolean flag equal to true
    
 //Course Number
 //Good
    System.out.println("What is your course number?"); 
    	//Prompts user for coarse number
    int coarseNumber; //Initiates course number to 0
    	if (myScanner.hasNextInt()){ //If myScanner returns an Integer than...
    		coarseNumber = myScanner.nextInt(); //Set that equal to coarse number
    	}
    	else {
    		Flag = false; //Else set Flag equal to false
    	}
        while (!Flag) { //While flag is false
    	    myScanner.next(); //Delete previous line
    	    System.out.println("What is your course number?");
    	    	//Re-asks for the course number
    	    if (myScanner.hasNextInt()){ //If myScanner returns an integer than...
        		coarseNumber = myScanner.nextInt(); //Then set equal to coarse number
        		Flag = true; //Then set flag equal to true, ends while statement
        	}
    	} 
        
 //Department Name
 //Good
    System.out.println("What is your department name?"); 
    	//Prompts user for their department name
    String departmentName; //Declares departmentName as an unknown string
        if (myScanner.hasNextInt() || myScanner.hasNextDouble()){ //If scanner returns an int or double than...
        	Flag = false; //Flag is false
        }
        else {
        	departmentName = myScanner.toString(); //Set equal to departmentName
        }
        while (!Flag) { //While flag is false
        	myScanner.next(); //Deletes last line
        	System.out.println("What is your department name?");
        		//Re-asks for the department name
        	if (!myScanner.hasNextInt() || !myScanner.hasNextDouble()){ //if input is not an int or double than
            	departmentName = myScanner.toString(); //set string equal to departmentName
            	Flag = true; //Set flag equal to true
            }
        } 
        
 //Meetings per Week    
    System.out.println("How many times a week do you meet?"); 
    	//Prompts user for amount of meetings per week
    int weeklyMeetings; //Initiates weekly meetings to 0
    	if (myScanner.hasNextInt()){ //If myScanner returns an Integer than...
    		weeklyMeetings = myScanner.nextInt(); //Set that equal to weekly meetings
    	}
    	else {
    		Flag = false; //Else set Flag equal to false
    	}
        while (!Flag) { //While flag is false
    	    myScanner.next(); //Delete previous line
    	    System.out.println("How many times a week do you meet?");
    	    	//Re-asks for the number of meetings per week
    	    if (myScanner.hasNextInt()){ //If myScanner returns an integer than...
        		weeklyMeetings = myScanner.nextInt(); //Then set equal to weekly meetings
        		Flag = true; //Then set flag equal to true, ends while statement
        	}
    	} 
        
  //Class Start Time   
     double meetingTime; //Initiates meeting time
     System.out.println("What time does class start? (Use a period instead of a colon)");
        	//Prompts user for the time class starts
        if (myScanner.hasNextDouble()){ //If myScanner returns an Integer than...
        	meetingTime = myScanner.nextDouble(); //Set that equal to class start time
        	}
        else {
        	Flag = false; //Else set Flag equal to false
        }
        while (!Flag) { //While flag is false
            myScanner.next(); //Delete previous line
        	System.out.println("What time does class start? (Use a period instead of a colon)");
        	    //Re-asks for the class start time
        	if (myScanner.hasNextDouble()){ //If myScanner returns an integer than...
        		meetingTime = myScanner.nextDouble(); //Then set equal to class start time
            	Flag = true; //Then set flag equal to true, ends while statement
            }
        }
        
  //Teacher Name 
  //Good
     System.out.println("What is your instructors name?"); 
           //Prompts user for their teachers name
     String teacherName; //Declares teacherName as an unknown string
        if (myScanner.hasNextInt() || myScanner.hasNextDouble()){ //If scanner returns an int or double than...
            Flag = false; //Flag is false
        }
        else {
            teacherName = myScanner.toString(); //Set equal to teacherName
        }
        while (!Flag) { //While flag is false
            myScanner.next(); //Deletes last line
            System.out.println("What is your instructors name?");
            	//Re-asks for the teacher name
        if (!myScanner.hasNextInt() || !myScanner.hasNextDouble()){ //if input is not an int or double than
            teacherName = myScanner.toString(); //set string equal to teacherName
            Flag = true; //Set flag equal to true
            }
        } 
        
  //Number of students
     System.out.println("How many students are in your class?"); 
           	//Prompts user for the number of students
     int studentNumber; //Initiates number of students
        if (myScanner.hasNextInt()){ //If myScanner returns an Integer than...
           		studentNumber = myScanner.nextInt(); //Set that equal to number of students
        }
        else {
            Flag = false; //Else set Flag equal to false
        }
        while (!Flag) { //While flag is false
           	myScanner.next(); //Delete previous line
           	System.out.println("How many students are in your class?");
           	    //Re-asks for the number of students
        if (myScanner.hasNextInt()){ //If myScanner returns an integer than...
               	studentNumber = myScanner.nextInt(); //Then set equal to number of students
               	Flag = true; //Then set flag equal to true, ends while statement
            }
        }    
	}
	
}
