import java.util.Scanner;
public class encrypted_x{
      //Main method required for every Java program
      public static void main(String[] args) {
  Scanner myScanner = new Scanner( System.in ); 
        //Tells Scanner that you are creating an instance that will take input from STDIN:
  System.out.println("Type an integer between 0-100"); 
    	//Prompts user for an integer
  	boolean Flag = true;
    int number = 0; //Initiates number to 0
    
    	if (myScanner.hasNextInt()){ //If myScanner returns an Integer than...    				
    		number = myScanner.nextInt();
    		if(number < 0 || number > 100) {
    			Flag = false;
    		}
    	}
    	else {
    		Flag = false;
    	}
    	
      while (!Flag) { //while not an int or less than 0
    	   myScanner.nextLine();
    	   System.out.println("Retype an integer please");
    	   if (myScanner.hasNextInt()){ //If myScanner returns an Integer than...    				
       	     number = myScanner.nextInt();
       	     if (number < 0 || number > 100) {
       	     Flag = false;
    	   }
    	   else {
    		   Flag = true;
        		}
      		}
    	}
      for(int i = 0; i < number; i++){
         for(int j = 0; j < number; j++){
           if (i == j || i == (number - (j +1))){
             System.out.print(" ");
           }
           else {
             System.out.print("*");
           }
         }
        System.out.println();
      }
   }
}
