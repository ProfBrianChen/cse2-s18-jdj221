import java.util.Scanner;
public class TwistGenerator{
      //Main method required for every Java program
      public static void main(String[] args) {
  Scanner myScanner = new Scanner( System.in ); 
        //Tells Scanner that you are creating an instance that will take input from STDIN:
  System.out.println("Type an integer"); 
    	//Prompts user for an integer
  	boolean Flag = true;
    int number = 0; //Initiates number to 0
    
    	if (myScanner.hasNextInt()){ //If myScanner returns an Integer than...    				
    		number = myScanner.nextInt();
    		if(number < 0) {
    			Flag = false;
    		}
    	}
    	else {
    		Flag = false;
    	}
    	
      while (!Flag) { //while not an int or less than 0
    	   myScanner.nextLine();
    	   System.out.println("Retype an integer");
    	   if (myScanner.hasNextInt()){ //If myScanner returns an Integer than...    				
       	     number = myScanner.nextInt();
       	     if (number < 0) {
       	     Flag = false;
    	   }
    	   else {
    		   Flag = true;
        		}
      		}
    	} 
  int twist = number / 3;
  int Remainder = number % 3;
  for (int i = 0; i < twist; i++){
        System.out.print("\\ /");
      }  
        if (Remainder == 1){
          System.out.print("\\");   
      }  
        if (Remainder == 2){
          System.out.print("\\ ");   
      }  
        System.out.print("\n");
  
  for (int i = 0; i < twist; i++){
        System.out.print(" X ");
      }  
        if (Remainder == 1){
          System.out.print(" ");   
      }  
        if (Remainder == 2){
          System.out.print(" X");   
      }  
        System.out.print("\n");

  for (int i = 0; i < twist; i++){
        System.out.print("/ \\");
      }
        if (Remainder == 1){
          System.out.print("/");   
      }  
        if (Remainder == 2){
          System.out.print("/  ");   
      }  
        System.out.print("\n");       
      }
}
