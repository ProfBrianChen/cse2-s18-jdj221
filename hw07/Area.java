import java.util.Scanner;
public class Area {
  public static double recArea(double width, double height){ //method for rectangle area
    double resultRec = (width * height);//formula for rectangle area
    return resultRec;
  }
  public static double triArea(double width, double height){ //method for triangle area
    double resultTri = ((width * height) / 2.0);//formula for triangle area
    return resultTri;
  }
  public static double circArea(double radius){ //method for circle area
    double resultCirc = ((3.14) * (radius) * (radius));//formula for area of a circle
    return resultCirc;
  }
    
      //Main method required for every Java program
      public static void main(String[] args) {
  Scanner myScanner = new Scanner( System.in ); 
        //Tells Scanner that you are creating an instance that will take input from STDIN:
        //Propts user for a number to determine what shape to find the area of
  System.out.println("Press 1 if you want to find area of a rectangle.");
  System.out.println("Press 2 if you want to find area of a triangle.");
  System.out.println("Press 3 if you want to find area of a circle.");
        
  int Answer = myScanner.nextInt(); //answer equals input
  
  //initiate variables
  double width = 0;
  double height = 0;
  double radius = 0;
  boolean Flag = true;     
  
      if (Answer == 1){ //if answer equals 1
        System.out.println("Input rectangle height");
        if (myScanner.hasNextDouble()){ //if input is adouble 
          height = myScanner.nextDouble();//set equal to height
        }
        else {
          Flag = false; //else flag is false
        }
        if (Flag = false){ //if flag is false
          myScanner.next();
          System.out.println("Re-enter rectangle height please");
          if (myScanner.hasNextDouble()){ //if input is a double 
            height = myScanner.nextDouble(); //heigtht is equal to input
            Flag = true; //flag equals true
        }
        }
        
        System.out.println("Input rectangle width");
        if (myScanner.hasNextDouble()){ //if double
          width = myScanner.nextDouble(); //width equals input
        }
        else {
          Flag = false; //else flag is false
        }
        if (Flag = false){ //if flag is false
          myScanner.next();
          System.out.println("Re-enter rectangle width please");
          if (myScanner.hasNextDouble()){ //if double
            width = myScanner.nextDouble(); //width equals input
            Flag = true; //flag is true
        }
        }
          double resultRec = recArea(width, height); //calls scanner
          System.out.println( resultRec ); //prints result
      }
      if (Answer == 2){  //if answer is 2
        System.out.println("Input triangle height");
        if (myScanner.hasNextDouble()){ //if double
          height = myScanner.nextDouble(); //height is equal to input
        }
        else {
          Flag = false; //flag is false
        }
        if (Flag = false){ //if flag is false
          myScanner.next();
          System.out.println("Re-enter triangle height please");
          if (myScanner.hasNextDouble()){ //if double
            height = myScanner.nextDouble();//height is equal to input
            Flag = true; //flag is true
        }
        }
        
        System.out.println("Input triangle width");
        if (myScanner.hasNextDouble()){ //if double
          width = myScanner.nextDouble(); //width equals input
        }
        else {
          Flag = false; //else flag is false
        }
        if (Flag = false){ //if flag is false
          myScanner.next();
          System.out.println("Re-enter triangle width please");
          if (myScanner.hasNextDouble()){ //if double
            width = myScanner.nextDouble(); //width equals input
            Flag = true; //flag is true
        }
        }
          double resultTri = triArea(width, height); //calls on method
          System.out.println( resultTri ); //prints result
      }
        
      if (Answer == 3){ //if answer is 3
        System.out.println("Input circle radius");
        if (myScanner.hasNextDouble()){ //if double
          radius = myScanner.nextDouble(); //radius is equal to input
        }
        else {
          Flag = false; //else flag is false
        }
        if (Flag = false){ //if flag is false
          myScanner.next();
          System.out.println("Re-enter circle radius please");
          if (myScanner.hasNextDouble()){ //if input is a double
            radius = myScanner.nextDouble(); //then radius is equal to input
            Flag = true; //and flag is true 
        }
        }
        double resultCirc = circArea( radius ); //calls method
        System.out.println( resultCirc ); //prints result
      }
}
}