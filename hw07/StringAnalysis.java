import java.util.Scanner;
public class StringAnalysis {
  public static boolean analysis( String answer1 ){
    int characters = answer1.length(); //sets characters equal to length of string
    boolean result = true; // result is true
    for (int i = 0; i < characters; i++ ){
      if ( answer1.charAt(i) >= 'a' && answer1.charAt(i) <= 'z'){ // if a letter
        result = true; // result is true 
      }
      else{
        result = false; //else result is false
        return result; //return result
      } 
    }
    return result; //return result
  }
  
   public static boolean analysis( String answer1, int answerChar ){ 
    boolean result2 = true; //result 2 is true 
    for (int i = 0; i < answerChar; i++ ){
      if ( answer1.charAt(i) >= 'a' && answer1.charAt(i) <= 'z'){ // if a letter
        result2 = true; //result 2 is true
      }
      else{
        result2 = false; //result 2 is false
        return result2; //return result 2
      } 
    }
    return result2; //return result 2
  }
  
  
      //Main method required for every Java program
      public static void main(String[] args) {
  Scanner myScanner = new Scanner( System.in ); 
        //Tells Scanner that you are creating an instance that will take input from STDIN:
        System.out.println("Input a string please");
        String answer1 = myScanner.next();
        System.out.println("Press 1 to process the whole string, or 2 to process part of it.");
        int answer2 = 0;
        int answerChar = 0;
        boolean Flag = true;
        boolean Flag1 = true;
          if (myScanner.hasNextInt()){
            answer2 = myScanner.nextInt();
            Flag = true;
          } 
            if (answer2 == 1){
              boolean result = analysis( answer1 ); //calls method
              System.out.println( "Your string is all letters: " + result ); //prints result
              Flag = true;
            }
            else if (answer2 == 2){
              System.out.println("How many characters would you like to evaluate?");
              answerChar = myScanner.nextInt();
              boolean result2 = analysis( answer1 ); //calls method
              System.out.println( "Your string is all letters: " + result2 ); //prints result
              Flag = true;
            }
            else {
              Flag = false;
            }
            while (Flag == false){
              //myScanner.next();
              System.out.println("Re-type your number please.");
              answer2 = myScanner.nextInt();
              if (answer2 == 1){
                boolean result = analysis( answer1 ); //calls method
                System.out.println( "Your string is all letters: " + result ); //prints result
                Flag = true;
            }
              else if (answer2 == 2){
                System.out.println("How many characters would you like to evaluate?");
                answerChar = myScanner.nextInt();
                boolean result2 = analysis( answer1 ); //calls method
                System.out.println( "Your string is all letters: " + result2 ); //prints result
                Flag = true;
            }
              else {
                Flag = false;
              }
            }
            
      }
}