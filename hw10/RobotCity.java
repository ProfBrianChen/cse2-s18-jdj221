import java.util.Random; // import random class
public class RobotCity{ //class Robot City
  public static int[][] buildCity(){
    int height = (int)(Math.random()*5 + 10);
    int width = (int)(Math.random()*5 + 10);
    int[][] cityArray = new int[height][width];
    for (int i = 0; i < height; i++){
      for (int j = 0; j < width; j++){
        cityArray[i][j] = ((int)(Math.random()*900 + 100));
      }
    }
    return cityArray;
  }
  public static void display(int[][] array){
    int u = 0;
    for (int i = 0; i < array.length; i++){
      System.out.print("{");
      for (int j = 0; j < array[0].length; j++){
        u = array[i][j];
        System.out.printf("%5d", u);
      }
      System.out.println(" }");
    } 
  }
  
  public static int[][] invade(int[][] array, int k){
    for (int g = k; g > 0; g--){
      int i = ((int)((Math.random()*array.length)));
      int j = ((int)((Math.random()*array[0].length)));
        if (array[i][j] > 0){
          array[i][j] = (-1*array[i][j]);
        }
        else {
          i++;
        }
    }
    return array;
  }
  
  public static int[][] update(int[][] array){
    for (int i = 0; i < array.length; i++){
      for (int j = 0; j < array[0].length; j++){
        if(array[i][j] < 0 && ((j+1) < array[0].length)){
          array[i][j] = (-1 * array[i][j]);
          if (array[i][j+1] <= 0){
            array[(i)][j+1] = (array[(i)][j+1]);
          }
          else{
            array[(i)][j+1] = (-1*array[(i)][j+1]);
          }
          j++;
        }
        else if (array[i][j] < 0 && (j+1) == array[0].length){
        array[i][j] = (-1 * array[i][j]); 
        }
        else {
          array[i][j] = array[i][j]; 
      }
     }
    }
    return array;
  }
  
  public static void main(String[] args){
//     int height = (int)(Math.random()*5 + 10);
//     int width = (int)(Math.random()*5 + 10);
//     int[][] cityArray = new int[height][width];
//     int k = ((int)((Math.random()*50) + 1));
//     for (int i = 0; i < height; i++){
//       for (int j = 0; j < width; j++){
//         cityArray[i][j] = ((int)(Math.random()*900 + 100));
//       }
//     }
    int k = ((int)((Math.random()*50) + 1));
    int[][] cityArray = buildCity(); 
    display(cityArray);
    invade(cityArray, k);
    System.out.println("Aliens have invaded!");
    display(cityArray);
    System.out.println("");
    int[][] array2 = cityArray;
    for (int i = 0; i < 5; i++){
    System.out.println("Alien Update: " + (i+1));
    update(array2);
    display(array2);
    }
  }
}