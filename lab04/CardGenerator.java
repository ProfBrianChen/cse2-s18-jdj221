// James Johnson
////// CARD GENERATOR //////
//Creating a program that picks a random card from a deck and 
//Identifies the card number and suite
public class CardGenerator{
      //Main method required for every Java program
      public static void main(String[] args) {
int cardnumber = (int)(Math.random()*52+1);
        //Assigning the random variable
int remainder = (int)(cardnumber%13);
        //Defines the remainder value in order to identify card value
String remainder2 = "";
        //Stores the card value 
String suit = "";
        //Stores the suit
  if (cardnumber >= 1 && cardnumber <= 13){ 
    suit = "Diamonds";
    //Assigns diamonds as card numbers 1-13
}
  if (cardnumber > 13 && cardnumber <= 26){ 
    suit = "Clubs";
    //Assigns clubs as card numbers 14-26
}
  if (cardnumber > 26 && cardnumber <= 39){ 
    suit = "Hearts";
    //Assigns hearts as card numbers 27-39  
}
  if (cardnumber > 39 && cardnumber <= 52){ 
    suit = "Spades";
    //Assigns spades as card numbers 40-52
}
        switch( remainder ){
            //Assigns all possible card values to remainder2
    case 1:
      remainder2 = "Ace";
    break;
    case 2:
      remainder2 = "2";
    break;
    case 3:
      remainder2 = "3";
    break;
    case 4:
      remainder2 = "4";
    break;
    case 5:
      remainder2 = "5";
    break;
    case 6:
      remainder2 = "6";
    break;
    case 7:
      remainder2 = "7";
    break;
    case 8:
      remainder2 = "8";
    break;
    case 9:
      remainder2 = "9";
    break;
    case 10:
      remainder2 = "10";
    break;
    case 11:
      remainder2 = "Jack";
    break;
    case 12:
      remainder2 = "Queen";
    break;
    case 13:
      remainder2 = "King";
    break;
        }  
    System.out.println("You picked a " + remainder2 + " of " + suit);
        //Prints the card value and suit that was generated
      }
}

                
                



        
   