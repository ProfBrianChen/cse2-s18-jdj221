import java.util.Scanner;
public class Argyle {
      //Main method required for every Java program
      public static void main(String[] args) {
  Scanner myScanner = new Scanner( System.in ); 
        //Tells Scanner that you are creating an instance that will take input from STDIN:
  System.out.println("Please enter a positive integer for the width of viewing window in characters."); 
    	//Prompts user for an integer
  	boolean Flag = true;
    int width = 0; //Initiates width
    
    	if (myScanner.hasNextInt()){ //If myScanner returns an Integer greater than 0 then...    				
    		width = myScanner.nextInt(); //width equals input
    		if (width <= 0) { //if width is less than or equal to 0
    			Flag = false; //than flag is false
    		}
        }
    	else {
    		Flag = false; //else flag is false
    	}
        while (Flag == false) { //while not an integer
           Flag = true; //flag is true
           myScanner.next(); // opens scanner on next line
    	   System.out.println("Retype an integer for the width please");
    	   if (myScanner.hasNextInt()){ //If myScanner returns an Integer greater than 0 then...    				
       		width = myScanner.nextInt(); //width equals input
       		if (width <= 0) { //if width his less than or equal to 0
       			Flag = false; // then flag equal false
       		}	
           }
       	else {
       		Flag = false; //else flag equal false
       	}
       }
     System.out.println("The width is " + width );
        
     System.out.println("Please enter a positive integer for the height of viewing window in characters");
     int height = 0; //Initiates height to 0
     boolean Flag1 = true; //sets boolean flag equal to 0
        
 	if (myScanner.hasNextInt()){ //If myScanner returns an Integer greater than 0 then...    				
		height = myScanner.nextInt();//height equals input
		if (height <= 0) { //if height is less than or equal to 0
			Flag1 = false; //than flag equals false
		}
    }
	else {
		Flag1 = false; //else flag equals false
	}
    while (Flag1 == false) { //while not an integer
       Flag1 = true; // flag equal true
       myScanner.next(); //opens scanner
	   System.out.println("Retype an integer for the height please");
	   if (myScanner.hasNextInt()){ //If myScanner returns an Integer greater than 0 then...    				
   		height = myScanner.nextInt(); // height equals input
   		if (height <= 0) { // if height is less than or equal to 0
   			Flag1 = false; // flag equals false
   		}	
       }
   	else {
   		Flag1 = false; // flag equals false
   	}
   }
 System.out.println("The height is " + height );
     
     
     System.out.println("Please enter a positive integer for the width of the argyle diamonds.");
     int argyleSize = 0; //Initiates height to 0
     boolean Flag2 = true;
        
     if (myScanner.hasNextInt()){ //If myScanner returns an Integer greater than 0 then...    				
 		argyleSize = myScanner.nextInt();//argyle size equals input
 		if (height <= 0) { //if argyle size is less than or equal to 0
 			Flag2 = false; //than flag equals false
 		}
     }
 	else {
 		Flag2 = false; //else flag equals false
 	}
     while (Flag2 == false) { //while not an integer
        Flag2 = true; // flag equal true
        myScanner.next(); //opens scanner
 	   System.out.println("Retype an integer for the argyle size please");
 	   if (myScanner.hasNextInt()){ //If myScanner returns an Integer greater than 0 then...    				
    		argyleSize = myScanner.nextInt(); //Argyle height equals input
    		if (height <= 0) { // if argyle size is less than or equal to 0
    			Flag2 = false; // flag equals false
    		}	
        }
    	else {
    		Flag2 = false; // flag equals false
    	}
    }
  System.out.println("The argyle size is " + argyleSize );
     
     
     System.out.println("Please enter a positive odd integer for the width of the argyle center stripe.");
     int argyleWidth = 0; //Initiates height to 0
     boolean Flag3 = true;
     
     if (myScanner.hasNextInt()){ //If myScanner returns an Integer greater than 0 then...    				
 		argyleWidth = myScanner.nextInt();//argyle size equals input
 		if (argyleWidth <= 0 || argyleWidth > (argyleSize /2) || argyleWidth % 2 == 0) { 
 			//if argyle width is less than or equal to 0, greater than half of the argyle size, or even...
 			Flag3 = false; //than flag equals false
 		}
     }
 	else {
 		Flag3 = false; //else flag equals false
 	}
     while (Flag3 == false) { //while not an integer
       Flag3 = true; // flag equal true
 	   System.out.println("Retype an integer for the argyle size please");
 	  if (myScanner.hasNextInt()){ //If myScanner returns an Integer greater than 0 then...    				
 	 		argyleWidth = myScanner.nextInt();//argyle size equals input
 	 		if (argyleWidth <= 0 || argyleWidth > (argyleSize /2) || argyleWidth % 2 == 0) { 
 	 			//if argyle width is less than or equal to 0, greater than half of the argyle size, or even...
 	 			Flag3 = false; //than flag equals false
 	 		}
 	     }
    	else {
    		Flag3 = false; // flag equals false
    		myScanner.next();
    	}
    }
  System.out.println("The argyle width is " + argyleWidth );
   
     
     System.out.println("Please enter a first character for the pattern fill.");
     String character1 = myScanner.next(); //Defines character1 as the input 
		char char1 = character1.charAt(0); //defines char1 as the first character in the string
		System.out.println("Character 1 is " + char1);
		
	 System.out.println("Please enter a second character for the pattern fill.");	
	 String character2 = myScanner.next(); //Defines character2 as the input
		char char2 = character2.charAt(0);//defines char 2 as the first character in the string
		System.out.println("Character 2 is " + char2);

	 System.out.println("Please enter a third character for the stripe fill.");
	 String character3 = myScanner.next();//Defines character3 as the input
		char char3 = character3.charAt(0);//Defines char Stripe as the first character in the string
		System.out.println("Stripe character is " + char3);
		
		 
		int completeDiamondWidth = 0;        //creates variable completeDiamondWidth
    int diamondRemainderWidth = 0;       //creates variable diamondRemainderWidth
    int completeDiamondHeight = 0;        //creates variable completeDiamondHeight
    int diamondRemainderHeight = 0;       //creates variable diamondRemainderHeight
    
    completeDiamondWidth = width / (2 * argyleSize); 
    diamondRemainderWidth = width % (2 * argyleSize);
    
    completeDiamondHeight = height / (2 * argyleSize);
    diamondRemainderHeight = height % (2 * argyleSize);
    
    for (int h = 0; h < completeDiamondHeight; h++){
			//Number of complete diamonds(height)
      for (int i = 0; i < (2 * argyleSize); i++){ 
				//Number of rows
        for(int d = 0; d < completeDiamondWidth; d++){ 
					//Number of complete diamonds(width)
          for (int j = 0; j < (2 * argyleSize); j++){ // prints characters
           if (i == j){
              System.out.print(char3);  //prints char3
            }
            else if ((argyleWidth > 1) && ( i - (argyleWidth -2) == j )){ //prints char3
              System.out.print(char3);
            }
            else if ((argyleWidth > 1) && ( i == j - (argyleWidth - 2))){ //prints char3
              System.out.print(char3);
            }
            else if ((i + j) == (2 * argyleSize - (argyleWidth - 2))){ //prints char3
              System.out.print(char3);
            }
            else if ((argyleWidth > 1) && ((i+j) == (2 * argyleSize - (argyleWidth - 3)))){ //prints char3
              System.out.print(char3);
            }
            else if ((argyleWidth > 1) && ((i+j) == (2 * argyleSize - (argyleWidth - 1)))){ //prints char3
              System.out.print(char3);
            }
            else if ((((i+j) >= (argyleSize - 1)) && ((i+j) < (3 * argyleSize ))) && (((j-i) <= (argyleSize)) && ((j-i) >= (-1*argyleSize)))) {
              System.out.print(char2); //prints char2
            }
            else {
              System.out.print(char1); //prints char1
            }
          } 
        } 

        for (int r = 0; r < diamondRemainderWidth; r++){ 
					//prints remainder for diamonds
            if (i == r){
              System.out.print(char3);  //prints char3
            }
            else if ((argyleWidth > 1) && ( i - (argyleWidth -2) == r )){ //prints char3
              System.out.print(char3);
            }
            else if ((argyleWidth > 1) && ( i == r - (argyleWidth - 2))){ //prints char3
              System.out.print(char3);
            }
            else if ((i + r) == (2 * argyleSize - (argyleWidth - 2))){ //prints char3
              System.out.print(char3);
            }
            else if ((argyleWidth > 1) && ((i+r) == (2 * argyleSize - (argyleWidth - 3)))){ //prints char3
              System.out.print(char3);
            }
            else if ((argyleWidth > 1) && ((i+r) == (2 * argyleSize - (argyleWidth - 1)))){ //prints char3
              System.out.print(char3);
            }
            else if ((((i+r) > (argyleSize - 1)) && ((i+r) < (3 * argyleSize ))) && (((r-i) <= (argyleSize)) && ((r-i) >= (-1*argyleSize)))) {
              System.out.print(char2); //prints char2
            }
            else {
              System.out.print(char1); //prints char1
            }
          } 

        System.out.print("\n");
      } 
    }
    for (int i = 0; i < diamondRemainderHeight; i++){ //Remainder in terms of height
        for(int d = 0; d < completeDiamondWidth; d++){ //Number of complete diamonds
          for (int j = 0; j < (2 * argyleSize); j++){ // prints characters
            if (i == j){ //prints char3
              System.out.print(char3);
            }
            else if ((argyleWidth > 1) && ( i - (argyleWidth -2) == j )){ //prints char3
              System.out.print(char3);
            }
            else if ((argyleWidth > 1) && ( i == j - (argyleWidth - 2))){ //prints char3
              System.out.print(char3);
            }
            else if ((i + j) == (2 * argyleSize - (argyleWidth - 2))){ //prints char3
              System.out.print(char3);
            }
            else if ((argyleWidth > 1) && ((i+j) == (2 * argyleSize - (argyleWidth - 3)))){ //prints char3
              System.out.print(char3);
            }
            else if ((argyleWidth > 1) && ((i+j) == (2 * argyleSize - (argyleWidth - 1)))){ //prints char3
              System.out.print(char3);
            }
            else if ((((i+j) >= (argyleSize - 1)) && ((i+j) < (3 * argyleSize ))) && (((j-i) <= (argyleSize)) && ((j-i) >= (-1*argyleSize)))) {
              System.out.print(char2); //prints char2
            }
            else {
              System.out.print(char1); //prints char1
            }
          } 
        } 

        for (int r = 0; r < diamondRemainderWidth; r++){ //Prints remainder for diamonds
            if (i == r){    //prints char3
              System.out.print(char3);
            }
            else if ((argyleWidth > 1) && ( i - (argyleWidth -2) == r )){ //prints char3
              System.out.print(char3);
            }
            else if ((argyleWidth > 1) && ( i == r - (argyleWidth - 2))){ //prints char3
              System.out.print(char3);
            }
            else if ((i + r) == (2 * argyleSize - (argyleWidth - 2))){ //prints char3
              System.out.print(char3);
            }
            else if ((argyleWidth > 1) && ((i+r) == (2 * argyleSize - (argyleWidth - 3)))){ //prints char3
              System.out.print(char3);
            }
            else if ((argyleWidth > 1) && ((i+r) == (2 * argyleSize - (argyleWidth - 1)))){ //prints char3
              System.out.print(char3);
            }
            else if ((((i+r) >= (argyleSize - 1)) && ((i+r) < (3 * argyleSize ))) && (((r-i) <= (argyleSize)) && ((r-i) >= (-1*argyleSize)))) {
              System.out.print(char2); //prints char2
            }
            else {
              System.out.print(char1); //prints char1
            }
          } 

        System.out.print("\n"); 
    }
    System.out.print("\n");
  } 
} 
