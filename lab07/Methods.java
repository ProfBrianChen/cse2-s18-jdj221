import java.util.Random; // import random class
import java.util.Scanner; // import scanner class

public class Methods{ // main class
 
  
  
  public static String adjective(){
     Random randomGenerator = new Random();
    String adj = "";
    int word1 = randomGenerator.nextInt(10);
    switch (word1){
      case 1: adj = "funny";
        break;
      case 2: adj = "wet";
        break;
      case 3: adj = "fat";
        break;
      case 4: adj = "rubber";
        break;
      case 5: adj = "slow";
        break;
      case 6: adj = "glowing";
        break;
      case 7: adj = "gold";
        break;
      case 8: adj = "drunk";
        break;
      case 9: adj = "exeptional";
        break;
    }
    return adj;
  }
  
    public static String subject(){
       Random randomGenerator = new Random();
    String subject = "";
    int word2 = randomGenerator.nextInt(10);;
    switch (word2){
      case 1: subject = "dog";
        break;
      case 2: subject = "person";
        break;
      case 3: subject = "blubber";
        break;
      case 4: subject = "boat";
        break;
      case 5: subject = "surfer";
        break;
      case 6: subject = "tiger";
        break;
      case 7: subject = "servant";
        break;
      case 8: subject = "serf";
        break;
      case 9: subject = "chair";
        break;
    }
    return subject;
  }
  
     public static String verb(){
        Random randomGenerator = new Random();
    String verb = "";
    int word3 = randomGenerator.nextInt(10);;
    switch (word3){
      case 1: verb = "pooped";
        break;
      case 2: verb = "skipped";
        break;
      case 3: verb = "lunged";
        break;
      case 4: verb = "partied";
        break;
      case 5: verb = "danced";
        break;
      case 6: verb = "shuffled";
        break;
      case 7: verb = "glared";
        break;
      case 8: verb = "slayed";
        break;
      case 9: verb = "slumped";
        break;
    }
    return verb;
  }
  
    public static String object(){
       Random randomGenerator = new Random();
    String object = "";
    int word4 = randomGenerator.nextInt(10);;
    switch (word4){
      case 1: object = "cat";
        break;
      case 2: object = "tree";
        break;
      case 3: object = "skin";
        break;
      case 4: object = "car";
        break;
      case 5: object = "couch potatoe";
        break;
      case 6: object = "lion";
        break;
      case 7: object = "dragon";
        break;
      case 8: object = "sword";
        break;
      case 9: object = "model";
        break;
    }
    return object;
  }
  
       public static String verb2(){
          Random randomGenerator = new Random();
    String verb2 = "";
    int word5 = randomGenerator.nextInt(10);;
    switch (word5){
      case 1: verb2 = "hopped";
        break;
      case 2: verb2 = "scattered";
        break;
      case 3: verb2 = "ran";
        break;
      case 4: verb2 = "ducked";
        break;
      case 5: verb2 = "swam";
        break;
      case 6: verb2 = "pitted";
        break;
      case 7: verb2 = "hid";
        break;
      case 8: verb2 = "ate";
        break;
      case 9: verb2 = "jumped";
        break;
    }
    return verb2;
  }
  
 public static void structure1(String subject){
    System.out.println("This  " + subject + " was " + adjective() + " in school, so it " + verb() + " the " + object() + ".");
    return;
    
  }
  
  public static void structure2(String subject){
    System.out.println("The " + subject + " was known as a " + adjective() + " " + object() + ".");
    return;
  }
  
  public static void structure3(String subject){
    System.out.println("It " + verb() + " and " + verb2() + " 'for quite some time most days.");
    return;
  }
  
  public static void structure4(String subject){
    System.out.println("Many a " + object() + " and their " + subject() + "s" + " " + verb2() + " this " + subject + " with a " + object() + ".");
    return;
  }

  
  public static void StoryRun (int sentCount){ // will compile story body
     Random randomGenerator = new Random();
    String sentenceSubject = subject();
    int sentenceStructureCount = 0;
    System.out.println("Once upon a time, there was a " + sentenceSubject + ".");
    System.out.println("");
    for (int i = 0; i < sentCount; i++){
      sentenceStructureCount = (int) (Math.random() * 4 + 1);
      switch (sentenceStructureCount){
        case 1: structure1(sentenceSubject);
          break;
        case 2: structure2(sentenceSubject);
          break;
        case 3: structure3(sentenceSubject);
          break;
        case 4: structure4(sentenceSubject);
          break;
      }
    }
    System.out.println("");
    System.out.println("That ends the story of the " + adjective() + " " + sentenceSubject + ".");
  }
  
  
  public static void main (String [] args){ 
    Scanner myScanner = new Scanner (System.in);
    boolean Flag = true;
    int count = 0;
    
    System.out.print("How many sentences would you like to print? Enter a positive integer: ");
    if (myScanner.hasNextInt()){
      count = myScanner.nextInt();
    }
    else {
      myScanner.nextLine();
    }
    if (count < 1){
      Flag = false;
    }
    
    
    while (Flag == false){
      System.out.print("Please input a positive integer for the amount of sentences you would like to print: ");
      if (myScanner.hasNextInt()){
      count = myScanner.nextInt();
      }
      else {
        myScanner.nextLine();
      }
      if (count < 1){
        Flag = false;
      }
      else {
        Flag = true;
      }
     
      
    }
    StoryRun(count);
  }
    
    
}



