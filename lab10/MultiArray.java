import java.util.Scanner;
import java.util.Random; // import random class
public class MultiArray{
  public static int[][] increasingMatrix(int width, int height, boolean format){
    int[][] myArray = new int[height][width];
    if (format == true){
    int val1 = 1;
      for (int i = 0; i < height; i++){
        for( int j = 0; j < width; j++){
          myArray[i][j] = val1++;
        }
      }
    }
    else if (format == false){
      int u = 0;
      for (int i = 0; i < width; i++){
        for( int j = 0; j < height; j++){
          myArray[j][i] = u ++;
        }
      }
    }
   return myArray;
   }
  
  public static void printMatrix(int[][] myArray, boolean format){ 
    if (format == true){
    int val1 = 1;
    int u = 0;
      for (int i = 0; i < myArray.length; i++){
        System.out.print("{");
        for( int j = 0; j < myArray[0].length; j++){
          myArray[i][j] = u++;
          System.out.print(" " + u);
        }
        System.out.println(" }");
      }
    }
    else if (format == false){
    int u = 0;
    //int[][] myArray = new int[height][];
      for (int i = 0; i < myArray.length; i++){
        System.out.print("{");
        for( int j = 0; j < myArray[0].length; j++){
          myArray[i][j] = u++;
          System.out.print(" " + u);
        }
        System.out.println(" }");
      }
    }
   }
  
  public static int[][] translate(int[][] array){
  int[][] array2 = new int[array.length][array[0].length];
    int u = 0;
  for (int i = 0; i < array.length; i++){
    System.out.print("{");
    for (int j = 0; j < array[0].length; j++){
      array2[i][j] = 1 + array[i][j];
      u = array2[i][j];
      System.out.print(" " + u);
    }
    System.out.println("}");
  }
  return array2;
}
  
  public static int[][] addMatrix(int[][] array1, boolean format1, int[][] array2, boolean format2) {
    int[][] newArray = new int[array1.length][array1[0].length];
    int u = 0;
    if((array1.length == array2.length) && (array1[0].length == array2[0].length)){
      for (int i = 0; i < array1.length; i++){
        System.out.print("{");
        for (int j = 0; j < array1[0].length; j++){
           newArray[i][j] = 2 + ((array1[i][j] + array2[i][j]));
           u = newArray[i][j];
           System.out.print(" " + u);
        }
        System.out.println("}");
      }
    return newArray;
    }
    else{
      return null;
    }
  }
  
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    Random randomGenerator = new Random();
    //System.out.println("Type an integer for width.");
    int width1 = (int)(Math.random()*8+1);
    System.out.println("Width: " + width1);
    //System.out.println("Type an integer for height.");
    int width2 = (int)(Math.random()*8+1);
    System.out.println("Width2: " + width2);
    boolean format1 = true;
    boolean format2 = true;
    int[][] myArray1 = increasingMatrix(width1, width1, format1);
    System.out.println("Matrix A looks like:");
    printMatrix(myArray1, format1);
    int[][] myArray2 = increasingMatrix(width1, width1, format2);
    System.out.println("Matrix B looks like:");
    printMatrix(myArray2, format1);
    int[][] myArray3 = increasingMatrix(width2, width2, format1);
    System.out.println("Matrix C looks like:");
    printMatrix(myArray3, format1);
    System.out.println("");
    System.out.println("A plus B is:");
    int[][] newArray1 = addMatrix(myArray1, format1, myArray2, format2);
    System.out.println("A plus C is:");
    int[][] newArray2 = addMatrix(myArray1, format1, myArray3, format2);
    if (newArray2 == null){
      System.out.println("Cannot print A and C");
    }
  }
}
  