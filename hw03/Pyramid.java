/// James Johnson
////// Pyramid //////
/// This homework will take inputs from the user to compute the volume of a pyramid
import java.util.Scanner;
public class Pyramid {
  // main method required for ever java program
  public static void main(String[] args) {
Scanner myScanner = new Scanner( System.in ); 
  //tells Scanner that you are creating an instance that will take input from STDIN:
System.out.print("Enter the square side length of the pyramid in the form xx.xx: ");
        //prompt user for the side length
double sideLength = myScanner.nextDouble();
        //Assigns as a double
System.out.print("Enter the height of the pyramid in the form xx.xx: ");
        //prompt user for the height
double shapeHeight = myScanner.nextDouble();
        //Assigns as a double
double pyramidVolume;
        //Assigns the Volume as a double
pyramidVolume = sideLength * sideLength * shapeHeight / 3;
        //Assigns pyramidVolume's volume
System.out.println("The Volume of the pyramid is " + pyramidVolume + "." );
  }
}