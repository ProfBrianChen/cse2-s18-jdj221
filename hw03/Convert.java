/// James Johnson
////// Convert //////
// This homework will convert the quantity of rain into cubic miles
import java.util.Scanner;
public class Convert {
  // main method required for ever java program
  public static void main(String[] args) {
Scanner myScanner = new Scanner( System.in ); 
  //tells Scanner that you are creating an instance that will take input from STDIN:
    System.out.print("Enter the affected area in acres: ");
        //prompt user for the number of acres affected by rainfall
double acresOfRain = myScanner.nextDouble();
        //Assigns as a double
    System.out.print("Enter the rainfall in the affected area: ");
        //prompt user for inches of rain dropped
double averageRainDropped = myScanner.nextDouble();
        //Assigns as a double
double amountOfRain;
    amountOfRain = averageRainDropped * acresOfRain;
    //gives total amount of rain in inches
double amountOfRainGallons;
    amountOfRainGallons =  ( (amountOfRain) / 1728 ) * 7.48052 ; //Formula to convert inches to gallons
double amountOfRainFinal;
    amountOfRainFinal = amountOfRainGallons / 1101117147428.6; //This is the amount of gallons in a cubic mile
System.out.println("The total amount of rain in cubic miles is " + amountOfRainFinal + "." );
  }
}
    
