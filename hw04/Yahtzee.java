// James Johnson
////// Yahtzee Game //////
//In this code I am generating a program using random variables
//to create a simulation of the game Yahtzee
import java.util.Scanner;
public class Yahtzee{
      //Main method required for every Java program
      public static void main(String[] args) {
  Scanner myScanner = new Scanner( System.in ); 
        //Tells Scanner that you are creating an instance that will take input from STDIN:
//Sets each values total count to 0
int Aces = 0; //Assigns aces equal to 0
int Twos = 0; //Assigns twos equal to 0
int Threes = 0; //Assigns threes equal to 0
int Fours = 0; //Assigns fours equal to 0
int Fives = 0; //Assignes fives equal to 0
int Sixes = 0; //Assignes sixes equal to 0

int Dice1 = (int)(Math.random()*6+1);
        //Assigning dice1 as a random variable  
int Dice2 = (int)(Math.random()*6+1);
        //Assigning dice2 as a random variable
int Dice3 = (int)(Math.random()*6+1);
        //Assigning dice3 as a random variable
int Dice4 = (int)(Math.random()*6+1);
        //Assigning dice4 as a random variable
int Dice5 = (int)(Math.random()*6+1);
        //Assigning dice5 as a random variable
System.out.println("Press 1 to roll randomly.");
System.out.println("Press 2 to pick your own number.");
        //Gives the user options on how to get dice values
int Answer = myScanner.nextInt();
        //Assigns inputed value as an integer
////// Prints dice values for the random roll option //////
if (Answer == 1){ 
  //System.out.println(Dice1); //Prints dice1 value
  //System.out.println(Dice2); //Prints dice2 value
  //System.out.println(Dice3); //Prints dice3 value
  //System.out.println(Dice4); //Prints dice4 value
  //System.out.println(Dice5); //Prints dice5 value
  
  switch( Dice1 ){
            //Adds together the total for each type
    case 1:
      Aces++;
    break;
    case 2:
      Twos++;
    break;
    case 3:
      Threes++;
    break;
    case 4:
      Fours++;
    break;
    case 5:
      Fives++;
    break;
    case 6:
      Sixes++;
    break;
}
switch( Dice2 ){
            //Adds together the total for each type
    case 1:
      Aces++;
    break;
    case 2:
      Twos++;
    break;
    case 3:
      Threes++;
    break;
    case 4:
      Fours++;
    break;
    case 5:
      Fives++;
    break;
    case 6:
      Sixes++;
    break;
}
switch( Dice3 ){
            //Adds together the total for each type
    case 1:
      Aces++;
    break;
    case 2:
      Twos++;
    break;
    case 3:
      Threes++;
    break;
    case 4:
      Fours++;
    break;
    case 5:
      Fives++;
    break;
    case 6:
      Sixes++;
    break;
}
switch( Dice4 ){
            //Adds together the total for each type
    case 1:
      Aces++;
    break;
    case 2:
      Twos++;
    break;
    case 3:
      Threes++;
    break;
    case 4:
      Fours++;
    break;
    case 5:
      Fives++;
    break;
    case 6:
      Sixes++;
    break;
}
switch( Dice5 ){
            //Adds together the total for each type
    case 1:
      Aces++;
    break;
    case 2:
      Twos++;
    break;
    case 3:
      Threes++;
    break;
    case 4:
      Fours++;
    break;
    case 5:
      Fives++;
    break;
    case 6:
      Sixes++;
    break;
}

  //Prints out how many of each type the user rolled
System.out.println("You rolled " + Aces + " aces.");
System.out.println("You rolled " + Twos + " twos.");
System.out.println("You rolled " + Threes + " threes.");
System.out.println("You rolled " + Fours + " fours.");
System.out.println("You rolled " + Fives +  " fives.");
System.out.println("You rolled " + Sixes +  " sixes.");
  
//Assigns the total value of each type 
int AcesTotal = (int)(Aces * 1); //Find the total value of aces
int TwosTotal = (int)(Twos * 2); //Find the total value of twos
int ThreesTotal = (int)(Threes * 3); //Find the total value of threes
int FoursTotal = (int)(Fours * 4); //Find the total value of fours
int FivesTotal = (int)(Fives * 5); //Find the total value of fives
int SixesTotal = (int)(Sixes * 6); //Find the total value of sixes
int Total_Dice_Value = (int)(AcesTotal + TwosTotal + ThreesTotal + FoursTotal + FivesTotal + SixesTotal);
  //Assigns the total count equal to the sum of all the individual die counts
int Total_Dice_Value1 = 0; //Bonus value is initialized as 0

//If total dice count is greater than or equal to 63, then add 35
if (Total_Dice_Value >= 63){
Total_Dice_Value1 = (Total_Dice_Value + 35);
}
//Else the bonus total value is the same as the total count
else {
  Total_Dice_Value1 = Total_Dice_Value;
}
 
//Prints out the score for each type 
//System.out.println("Aces score is " + AcesTotal);
//System.out.println("Twos score is " + TwosTotal);
//System.out.println("Threes score is " + ThreesTotal);
//System.out.println("Fours score is " + FoursTotal);
//System.out.println("Fives score is " + FivesTotal);
//System.out.println("Sixes score is " + SixesTotal);
System.out.println("Your Upper Section Total Score is " + Total_Dice_Value);
  //Prints out total count for the Upper Section
System.out.println("Your Upper Section Total  Score with the bonus is " + Total_Dice_Value1);
  //Prints out the total count with bonus included for the upper section
  
  
  ///////// Starting the Lower Card Scoring /////////
//Initializes all 3 of a kind, full house, and straights values as 0
  
int ThreeAces = 0;
int ThreeTwos = 0;
int ThreeThrees = 0;
int ThreeFours = 0;
int ThreeFives = 0;
int ThreeSixes = 0;
int SmallStraight = 0;
int FullHouse = 0; //Initializing full house value equal to 0
int LargeStraight = 0;
  
//Straights
  if (((Aces == 1) && (Twos == 1) && (Threes == 1) && (Fours == 1)) || ((Twos == 1) && (Threes == 1) && (Fours == 1) && (Fives == 1)) || ((Threes == 1) && (Fours == 1) && (Fives != 1) && (Sixes != 1))){
    SmallStraight = 30;
    //System.out.println("You got a small straight");
    //System.out.println("You get " + SmallStraight + " points for a small straight!");
    //Prints points for a small straight
    if (((Aces == 1) && (Twos == 1) && (Threes == 1) && (Fours == 1) && (Fives ==1)) || ((Sixes == 1) && (Twos == 1) && (Threes == 1) && (Fours == 1) && (Fives ==1))){
    LargeStraight = 40;
    SmallStraight = 0;
    //System.out.println("You got a large straight");
    //System.out.println("Exclude the small straight, and you now get " + LargeStraight + " points for a large straight!");
    }
}

  if (Aces == 3){
    ThreeAces = (int)(3 * 1); //Sets equal to 3 times die value
    //System.out.println("You rolled 3 of a kind in aces!"); 
    //System.out.println("Your 3 of a kind score is " + ThreeAces);
      //Prints that you rolled a 3 of a kind and tells you your score 
    if ((Twos == 2) || (Threes == 2) || (Fours == 2) || (Fives == 2) || (Sixes == 2)){
      FullHouse = 25;
      ThreeAces = 0;
      //System.out.println("You got a full house, so instead of the points for 3 of a kind, you get 25 points!");
    }
  }
  
  if (Twos == 3){
    ThreeTwos = (int)(3 * 2); //Sets equal to 3 times die value
    //System.out.println("You rolled 3 of a kind in twos!");
    //System.out.println("Your 3 of a kind score is " + ThreeTwos);
      //Prints that you rolled a 3 of a kind and tells you your score 
    if ((Aces == 2) || (Threes == 2) || (Fours == 2) || (Fives == 2) || (Sixes == 2)){
      FullHouse = 25;
      ThreeTwos = 0;
      //System.out.println("You got a full house, so instead of the points for 3 of a kind, you get 25 points!");
    }
  }

  if (Threes == 3){
    ThreeThrees = (int)(3 * 3); //Sets equal to 3 times die value
    //System.out.println("You rolled 3 of a kind in threes!");
    //System.out.println("Your 3 of a kind score is " + ThreeThrees);
      //Prints that you rolled a 3 of a kind and tells you your score
    if ((Twos == 2) || (Aces == 2) || (Fours == 2) || (Fives == 2) || (Sixes == 2)){
      FullHouse = 25;
      ThreeThrees = 0;
      //System.out.println("You got a full house, so instead of the points for 3 of a kind, you get 25 points!");
    }
  }

  if (Fours == 3){
    ThreeFours = (int)(3 * 4); //Sets equal to 3 times die value
    //System.out.println("You rolled 3 of a kind in fours!");
    //System.out.println("Your 3 of a kind score is " + ThreeFours);
      //Prints that you rolled a 3 of a kind and tells you your score
    if ((Twos == 2) || (Threes == 2) || (Aces == 2) || (Fives == 2) || (Sixes == 2)){
      FullHouse = 25;
      ThreeFours = 0;
     // System.out.println("You got a full house, so instead of the points for 3 of a kind, you get 25 points!");
    }
  }

  if (Fives == 3){
    ThreeFives = (int)(3 * 5); //Sets equal to 3 times die value
    //System.out.println("You rolled 3 of a kind in fives!");
    //System.out.println("Your 3 of a kind score is " + ThreeFives);
      //Prints that you rolled a 3 of a kind and tells you your score 
    if ((Twos == 2) || (Threes == 2) || (Fours == 2) || (Aces == 2) || (Sixes == 2)){
      FullHouse = 25;
      ThreeFives = 0;
      //System.out.println("You got a full house, so instead of the points for 3 of a kind, you get 25 points!");
    }
  }
 
  if (Sixes == 3){
    ThreeSixes = (int)(3 * 6); //Sets equal to 3 times die value
    //System.out.println("You rolled 3 of a kind in sixes!");
    //System.out.println("Your 3 of a kind score is " + ThreeSixes);
      //Prints that you rolled a 3 of a kind and tells you your score 
    if ((Twos == 2) || (Threes == 2) || (Fours == 2) || (Fives == 2) || (Aces == 2)){
      FullHouse = 25;
      ThreeSixes = 0;
     // System.out.println("You got a full house, so instead of the points for 3 of a kind, you get 25 points!");
    }
  }
  
   //Initializes all 4 of a kind values as 0
  
int FourAces = 0;
int FourTwos = 0;
int FourThrees = 0;
int FourFours = 0;
int FourFives = 0;
int FourSixes = 0;
  
  if (Aces == 4){
    FourAces = (int)(4 * 1); //Sets equal to 4 times die value
    //System.out.println("You rolled 4 of a kind in aces!"); 
    //System.out.println("Your 4 of a kind score is " + FourAces);
      //Prints that you rolled a 4 of a kind and tells you your score 
  }
  
  if (Twos == 4){
    FourTwos = (int)(4 * 2); //Sets equal to 4 times die value
    //System.out.println("You rolled 4 of a kind in twos!");
    //System.out.println("Your 4 of a kind score is " + FourTwos);
      //Prints that you rolled a 4 of a kind and tells you your score 
  }
  
  if (Threes == 4){
    FourThrees = (int)(4 * 3); //Sets equal to 4 times die value
    //System.out.println("You rolled 4 of a kind in threes!");
    //System.out.println("Your 4 of a kind score is " + FourThrees);
      //Prints that you rolled a 4 of a kind and tells you your score 
  }
  
  if (Fours == 4){
    FourFours = (int)(4 * 4); //Sets equal to 4 times die value
    //System.out.println("You rolled 4 of a kind in fours!");
    //System.out.println("Your 4 of a kind score is " + FourFours);
      //Prints that you rolled a 4 of a kind and tells you your score 
  }
  
  if (Fives == 4){
    FourFives = (int)(4 * 5); //Sets equal to 4 times die value
    //System.out.println("You rolled 4 of a kind in fives!");
    //System.out.println("Your 4 of a kind score is " + FourFives);
      //Prints that you rolled a 4 of a kind and tells you your score 
  }
  
  if (Sixes == 4){
    FourSixes = (int)(4 * 6); //Sets equal to 4 times die value
    //System.out.println("You rolled 4 of a kind in sixes!");
    //System.out.println("Your 4 of a kind score is " + FourSixes);
      //Prints that you rolled a 4 of a kind and tells you your score 
  }
  
//Yahtzee
int Yahtzee = 0;
  if ((Aces == 5) || (Twos == 5) || (Threes == 5) || (Fours == 5) || (Sixes == 5)){
    //If any of the values is equal to 5...
    Yahtzee = 50; //50 points for a yahtzee
    //System.out.println("You rolled a Yahtzee!"); 
    //System.out.println("Your Yahtzee score is " + Yahtzee);
      //Prints that you rolled a Yahtzee and tells you your score 
  }
  
//Chance
int Chance = 0;
  if ((Yahtzee == 0) && (FourAces == 0) &&  (FourTwos == 0) && (FourThrees == 0) && (FourFours == 0))
  if ((FourFives == 0) && (FourSixes == 0) && (ThreeAces == 0) && (ThreeTwos == 0) && (ThreeThrees == 0))
  if ((ThreeFours == 0) && (ThreeFives == 0) && (ThreeSixes == 0) && (SmallStraight == 0))
  if ((FullHouse == 0) && (LargeStraight == 0)){
    Chance = (Dice1 + Dice2 + Dice3 + Dice4 + Dice5);
    //System.out.println("Chance score is " + Chance);
  }
  
  //Lower Section Total
  int LowerTotal = (SmallStraight + LargeStraight + FullHouse + Chance + Yahtzee + ThreeAces + ThreeTwos + ThreeThrees + ThreeFours + ThreeFives + ThreeSixes + FourAces + FourTwos + FourThrees + FourFours + FourFives + FourSixes);
  System.out.println("The total lower section value is " + LowerTotal);
                    
  int GrandTotal = (LowerTotal + Total_Dice_Value1);
  System.out.println("The grand total of your roll is " + GrandTotal);
      } //End if answer == 1
        
      
        
        
        
        
        
////// Assignes inputed value as an integer //////
if (Answer == 2){
  System.out.println("Write a 5 digit number with each digit being less than or equal to 6.");
  //Prints dice values for the user custom input option
int DiceValue = (int)myScanner.nextDouble();
if (DiceValue >= 100000){
  System.out.println("Error");
return;
}
if (DiceValue >= 10000){
//Uses modulus to find each dices value 
  Dice1 = (int)(DiceValue  / 10000) % 10;
  Dice2 = (int)(DiceValue  / 1000) % 10;
  Dice3 = (int)(DiceValue  / 100) % 10;
  Dice4 = (int)(DiceValue  / 10) % 10;  
  Dice5 = (int)DiceValue % 10;
  
if ((Dice1 > 6) || (Dice2 > 6) || (Dice3 > 6) || (Dice4 > 6) || (Dice5 > 6)){
  System.out.println("Error");
return;
}
  
//Prints out the dices values found using modulus  
  //System.out.println(Dice1);
  //System.out.println(Dice2);
  //System.out.println(Dice3);
  //System.out.println(Dice4);
  //System.out.println(Dice5);
  }

switch( Dice1 ){
            //Adds together the total for each type
    case 1:
      Aces++;
    break;
    case 2:
      Twos++;
    break;
    case 3:
      Threes++;
    break;
    case 4:
      Fours++;
    break;
    case 5:
      Fives++;
    break;
    case 6:
      Sixes++;
    break;
}
switch( Dice2 ){
            //Adds together the total for each type
    case 1:
      Aces++;
    break;
    case 2:
      Twos++;
    break;
    case 3:
      Threes++;
    break;
    case 4:
      Fours++;
    break;
    case 5:
      Fives++;
    break;
    case 6:
      Sixes++;
    break;
}
switch( Dice3 ){
            //Adds together the total for each type
    case 1:
      Aces++;
    break;
    case 2:
      Twos++;
    break;
    case 3:
      Threes++;
    break;
    case 4:
      Fours++;
    break;
    case 5:
      Fives++;
    break;
    case 6:
      Sixes++;
    break;
}
switch( Dice4 ){
            //Adds together the total for each type
    case 1:
      Aces++;
    break;
    case 2:
      Twos++;
    break;
    case 3:
      Threes++;
    break;
    case 4:
      Fours++;
    break;
    case 5:
      Fives++;
    break;
    case 6:
      Sixes++;
    break;
}
switch( Dice5 ){
            //Adds together the total for each type
    case 1:
      Aces++;
    break;
    case 2:
      Twos++;
    break;
    case 3:
      Threes++;
    break;
    case 4:
      Fours++;
    break;
    case 5:
      Fives++;
    break;
    case 6:
      Sixes++;
    break;
}
//Prints out how many of each type the user rolled
System.out.println("You rolled " + Aces + " aces.");
System.out.println("You rolled " + Twos + " twos.");
System.out.println("You rolled " + Threes + " threes.");
System.out.println("You rolled " + Fours + " fours.");
System.out.println("You rolled " + Fives +  " fives.");
System.out.println("You rolled " + Sixes +  " sixes.");
  
//Assigns the total value of each type 
int AcesTotal = (int)(Aces * 1); //Find the total value of aces
int TwosTotal = (int)(Twos * 2); //Find the total value of twos
int ThreesTotal = (int)(Threes * 3); //Find the total value of threes
int FoursTotal = (int)(Fours * 4); //Find the total value of fours
int FivesTotal = (int)(Fives * 5); //Find the total value of fives
int SixesTotal = (int)(Sixes * 6); //Find the total value of sixes
int Total_Dice_Value = (int)(AcesTotal + TwosTotal + ThreesTotal + FoursTotal + FivesTotal + SixesTotal);
  //Assigns the total count equal to the sum of all the individual die counts
int Total_Dice_Value1 = 0; //Bonus value is initialized as 0

//If total dice count is greater than or equal to 63, then add 35
if (Total_Dice_Value >= 63){
Total_Dice_Value1 = (Total_Dice_Value + 35);
}
//Else the bonus total value is the same as the total count
else {
  Total_Dice_Value1 = Total_Dice_Value;
}
 
//Prints out the score for each type 
//System.out.println("Aces score is " + AcesTotal);
//System.out.println("Twos score is " + TwosTotal);
//System.out.println("Threes score is " + ThreesTotal);
//System.out.println("Fours score is " + FoursTotal);
//System.out.println("Fives score is " + FivesTotal);
//System.out.println("Sixes score is " + SixesTotal);
System.out.println("Your Upper Section Total Score is " + Total_Dice_Value);
  //Prints out total count for the Upper Section
System.out.println("Your Upper Section Total  Score with the bonus is " + Total_Dice_Value1);
  //Prints out the total count with bonus included for the upper section
  
  
  ///////// Starting the Lower Card Scoring /////////
//Initializes all 3 of a kind, full house, and straights values as 0
  
int ThreeAces = 0;
int ThreeTwos = 0;
int ThreeThrees = 0;
int ThreeFours = 0;
int ThreeFives = 0;
int ThreeSixes = 0;
int SmallStraight = 0;
int FullHouse = 0; //Initializing full house value equal to 0
int LargeStraight = 0;
  
//Straights
  if (((Aces == 1) && (Twos == 1) && (Threes == 1) && (Fours == 1)) || ((Twos == 1) && (Threes == 1) && (Fours == 1) && (Fives == 1)) || ((Threes == 1) && (Fours == 1) && (Fives != 1) && (Sixes != 1))){
    SmallStraight = 30;
    //System.out.println("You got a small straight");
    //System.out.println("You get " + SmallStraight + " points for a small straight!");
    //Prints points for a small straight
    if (((Aces == 1) && (Twos == 1) && (Threes == 1) && (Fours == 1) && (Fives ==1)) || ((Sixes == 1) && (Twos == 1) && (Threes == 1) && (Fours == 1) && (Fives ==1))){
    LargeStraight = 40;
    SmallStraight = 0;
    //System.out.println("You got a large straight");
    //System.out.println("Exclude the small straight, and you now get " + LargeStraight + " points for a large straight!");
    }
}

  if (Aces == 3){
    ThreeAces = (int)(3 * 1); //Sets equal to 3 times die value
    //System.out.println("You rolled 3 of a kind in aces!"); 
    //System.out.println("Your 3 of a kind score is " + ThreeAces);
      //Prints that you rolled a 3 of a kind and tells you your score 
    if ((Twos == 2) || (Threes == 2) || (Fours == 2) || (Fives == 2) || (Sixes == 2)){
      FullHouse = 25;
      ThreeAces = 0;
     // System.out.println("You got a full house, so instead of the points for 3 of a kind, you get 25 points!");
    }
  }
  
  if (Twos == 3){
    ThreeTwos = (int)(3 * 2); //Sets equal to 3 times die value
    //System.out.println("You rolled 3 of a kind in twos!");
    //System.out.println("Your 3 of a kind score is " + ThreeTwos);
      //Prints that you rolled a 3 of a kind and tells you your score 
    if ((Aces == 2) || (Threes == 2) || (Fours == 2) || (Fives == 2) || (Sixes == 2)){
      FullHouse = 25;
      ThreeTwos = 0;
      //System.out.println("You got a full house, so instead of the points for 3 of a kind, you get 25 points!");
    }
  }

  if (Threes == 3){
    ThreeThrees = (int)(3 * 3); //Sets equal to 3 times die value
    //System.out.println("You rolled 3 of a kind in threes!");
    //System.out.println("Your 3 of a kind score is " + ThreeThrees);
      //Prints that you rolled a 3 of a kind and tells you your score
    if ((Twos == 2) || (Aces == 2) || (Fours == 2) || (Fives == 2) || (Sixes == 2)){
      FullHouse = 25;
      ThreeThrees = 0;
      //System.out.println("You got a full house, so instead of the points for 3 of a kind, you get 25 points!");
    }
  }

  if (Fours == 3){
    ThreeFours = (int)(3 * 4); //Sets equal to 3 times die value
    //System.out.println("You rolled 3 of a kind in fours!");
    //System.out.println("Your 3 of a kind score is " + ThreeFours);
      //Prints that you rolled a 3 of a kind and tells you your score
    if ((Twos == 2) || (Threes == 2) || (Aces == 2) || (Fives == 2) || (Sixes == 2)){
      FullHouse = 25;
      ThreeFours = 0;
      //System.out.println("You got a full house, so instead of the points for 3 of a kind, you get 25 points!");
    }
  }

  if (Fives == 3){
    ThreeFives = (int)(3 * 5); //Sets equal to 3 times die value
    //System.out.println("You rolled 3 of a kind in fives!");
    //System.out.println("Your 3 of a kind score is " + ThreeFives);
      //Prints that you rolled a 3 of a kind and tells you your score 
    if ((Twos == 2) || (Threes == 2) || (Fours == 2) || (Aces == 2) || (Sixes == 2)){
      FullHouse = 25;
      ThreeFives = 0;
      //System.out.println("You got a full house, so instead of the points for 3 of a kind, you get " + FullHouse + " points!");
    }
  }
 
  if (Sixes == 3){
    ThreeSixes = (int)(3 * 6); //Sets equal to 3 times die value
    //System.out.println("You rolled 3 of a kind in sixes!");
    //System.out.println("Your 3 of a kind score is " + ThreeSixes);
      //Prints that you rolled a 3 of a kind and tells you your score 
    if ((Twos == 2) || (Threes == 2) || (Fours == 2) || (Fives == 2) || (Aces == 2)){
      FullHouse = 25;
      ThreeSixes = 0;
      //System.out.println("You got a full house, so instead of the points for 3 of a kind, you get " + FullHouse + " points!");
    }
  }
  
   //Initializes all 4 of a kind values as 0
  
int FourAces = 0;
int FourTwos = 0;
int FourThrees = 0;
int FourFours = 0;
int FourFives = 0;
int FourSixes = 0;
  
  if (Aces == 4){
    FourAces = (int)(4 * 1); //Sets equal to 4 times die value
    //System.out.println("You rolled 4 of a kind in aces!"); 
    //System.out.println("Your 4 of a kind score is " + FourAces);
      //Prints that you rolled a 4 of a kind and tells you your score 
  }
  
  if (Twos == 4){
    FourTwos = (int)(4 * 2); //Sets equal to 4 times die value
    //System.out.println("You rolled 4 of a kind in twos!");
    //System.out.println("Your 4 of a kind score is " + FourTwos);
      //Prints that you rolled a 4 of a kind and tells you your score 
  }
  
  if (Threes == 4){
    FourThrees = (int)(4 * 3); //Sets equal to 4 times die value
    //System.out.println("You rolled 4 of a kind in threes!");
    //System.out.println("Your 4 of a kind score is " + FourThrees);
      //Prints that you rolled a 4 of a kind and tells you your score 
  }
  
  if (Fours == 4){
    FourFours = (int)(4 * 4); //Sets equal to 4 times die value
    //System.out.println("You rolled 4 of a kind in fours!");
    //System.out.println("Your 4 of a kind score is " + FourFours);
      //Prints that you rolled a 4 of a kind and tells you your score 
  }
  
  if (Fives == 4){
    FourFives = (int)(4 * 5); //Sets equal to 4 times die value
    //System.out.println("You rolled 4 of a kind in fives!");
    //System.out.println("Your 4 of a kind score is " + FourFives);
      //Prints that you rolled a 4 of a kind and tells you your score 
  }
  
  if (Sixes == 4){
    FourSixes = (int)(4 * 6); //Sets equal to 4 times die value
    //System.out.println("You rolled 4 of a kind in sixes!");
    //System.out.println("Your 4 of a kind score is " + FourSixes);
      //Prints that you rolled a 4 of a kind and tells you your score 
  }
  
//Yahtzee
int Yahtzee = 0;
  if ((Aces == 5) || (Twos == 5) || (Threes == 5) || (Fours == 5) || (Fives == 5) || (Sixes == 5)){
    //If any of the values is equal to 5...
    Yahtzee = 50; //50 points for a yahtzee
    //System.out.println("You rolled a Yahtzee!"); 
    //System.out.println("Your Yahtzee score is " + Yahtzee);
      //Prints that you rolled a Yahtzee and tells you your score 
  }
  
//Chance
int Chance = 0;
  if ((Yahtzee == 0) && (FourAces == 0) &&  (FourTwos == 0) && (FourThrees == 0) && (FourFours == 0))
  if ((FourFives == 0) && (FourSixes == 0) && (ThreeAces == 0) && (ThreeTwos == 0) && (ThreeThrees == 0))
  if ((ThreeFours == 0) && (ThreeFives == 0) && (ThreeSixes == 0) && (SmallStraight == 0))
  if ((FullHouse == 0) && (LargeStraight == 0)){
    Chance = (Dice1 + Dice2 + Dice3 + Dice4 + Dice5);
    //System.out.println("Chance score is " + Chance);
  }
  
  //Lower Section Total
  int LowerTotal = (SmallStraight + LargeStraight + FullHouse + Chance + Yahtzee + ThreeAces + ThreeTwos + ThreeThrees + ThreeFours + ThreeFives + ThreeSixes + FourAces + FourTwos + FourThrees + FourFours + FourFives + FourSixes);
  System.out.println("The total lower section value is " + LowerTotal);
                    
  int GrandTotal = (LowerTotal + Total_Dice_Value1);
  System.out.println("The grand total of your roll is " + GrandTotal);
      } //End if answer == 2  
    } //End main
  } //End class

       
