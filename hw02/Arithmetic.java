// James Johnson
////// Arithmetic //////
/// This lab will use basic arithmetic to calculate sales tax and cost of an order
public class Arithmetic {
  // main method required for ever java program
  public static void main(String[] args) {
//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per sweatshirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belts
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;
    
double totalCostOfPants;  
    //total cost of pants
double totalCostOfSweatshirts;  
    //total cost of sweatshirts
double totalCostOfBelts;  
    //total cost of belts
double totalCostOfOrder;  
    //total cost of order without tax
double totalCostOfOrder2;
    //total cost of order including tax
double totalCostOfTax;
    //amount of tax on total order
double totalTaxOnPants;
    //tax on pants
double totalTaxOnSweatshirts;
    //tax on sweatshirts
double totalTaxOnBelts;
    //tax on belts
totalCostOfPants = numPants * pantsPrice;
    //total cost of pants incuding tax
totalCostOfSweatshirts = numShirts * shirtPrice;
    //total cost of sweatshirts including tax
totalCostOfBelts = numBelts * beltCost;
    //total cost of belts including tax

totalCostOfOrder = totalCostOfPants + totalCostOfSweatshirts + totalCostOfBelts; 
    //total cost of order without tax
totalCostOfOrder2 = totalCostOfOrder * paSalesTax + totalCostOfOrder;
    //total cost of order with tax 

totalCostOfTax = totalCostOfOrder2 - totalCostOfOrder;
    //total amount of sales tax on the order
    
totalTaxOnPants = totalCostOfPants * paSalesTax;
    //total tax on the pants
totalTaxOnSweatshirts = totalCostOfSweatshirts * paSalesTax;
    //total tax on the sweatshirts
totalTaxOnBelts = totalCostOfBelts * paSalesTax;
    //total tax on the belts
// converting t an integer in oder to eliminate excesive decimal places 
 int pantsTax = (int) (paSalesTax * totalCostOfPants * 100);
 int shirtsTax = (int) (paSalesTax * totalCostOfSweatshirts * 100);
 int beltsTax = (int) (paSalesTax * totalCostOfBelts * 100);
 int totalTax = (int) (totalCostOfTax * 100);
 double TaxOnPants = pantsTax/100;
 double TaxOnSweatshirts = shirtsTax/100;
 double TaxOnBelts = beltsTax/100;
 double TaxTotal = totalTax/100;

System.out.print("Total cost of pants is:");
System.out.println(totalCostOfPants);
    //Prints cost of pants
System.out.print("Total cost of sweatshirts is:");
System.out.println(totalCostOfSweatshirts);
    //Prints cost of sweatshirts
System.out.print("Total cost of belts is:");
System.out.println(totalCostOfBelts);
    //Prints cost of belts
System.out.print("Sales tax on pants is:");
System.out.println(TaxOnPants);
    //Prints amount of tax on pants
System.out.print("Sales tax on sweatshirts is:");
System.out.println(TaxOnSweatshirts);
    //Prints amount of tax on sweatshirts
System.out.print("Sales tax on belts is:");
System.out.println(TaxOnBelts);
    //Prints amount of tax on belts
System.out.print("Total cost of purchase before tax:");
System.out.println(totalCostOfOrder);
    //Prints amount of order before tax
System.out.print("Total amount of sales tax is:");
System.out.println(TaxTotal);
    //Prints amount of tax
System.out.print("Total cost of purchase after tax:");
System.out.println(totalCostOfOrder2);
    //Prints amount of order after tax
  }
}

