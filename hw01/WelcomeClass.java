////////////////////
/////// HW WelcomeClass ////////
///
public class WelcomeClass { 
  // main method required for ever java program
  public static void main(String[] args) { //
  // System is printing out lines 
    System.out.println(" ----------- ");
    System.out.println(" | WELCOME | ");
    System.out.println(" ----------- ");
    System.out.println(" ^  ^  ^  ^  ^  ^ ");
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-J--D--J--2--2--1->");
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println(" v  v  v  v  v  v");
    // Now I'm printing my short Biography
    System.out.println("I am a freshman CSB student");
    System.out.println("I love to play guitar and surf");
    System.out.println("I am from San Diego, California");                 
  } //ends main method
} // ends class