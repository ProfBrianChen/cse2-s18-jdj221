// James Johnson
// 2/2/18
/////// Cyclometer Lab /////////
// this lab will:
// print number of minutes per trip 
// print number of counts per trip
// print distance of each trip in miles
// print total distance 
public class Cyclometer {
  // main method required for ever java program
  public static void main(String[] args) {
    int secsTrip1=480; // minutes for trip 1
    int secsTrip2=3220; // minutes for trip 2
    int countsTrip1=1561; // counts for trip 1
    int countsTrip2=9037; // counts for trip 2
    double wheelDiameter=27.0; //wheel diameter is 27 
    double PI=3.14159; // define PI
    int feetPerMile=5280; // length of mile
    int inchesPerFoot=12; // inches per food 
    int secondsPerMinute=60; // length of minute
    double distanceTrip1, distanceTrip2, totalDistance; // define trip distance as a double
    System.out.println("Trip 1 took "+
                (secsTrip1/secondsPerMinute)+" minutes and had "+
                 countsTrip1+" counts.");
           System.out.println("Trip 2 took "+
                (secsTrip2/secondsPerMinute)+" minutes and had "+
                 countsTrip2+" counts.");
    // Running calculations below
    // Calculating distance in inches and distance in miles
    distanceTrip1=countsTrip1*wheelDiameter*PI;
        // Distance in inches
        //(for each count, a rotation of the wheel travels
        //the diameter in inches times PI)
    distanceTrip1/=inchesPerFoot*feetPerMile; // Distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;
    // Print out the output data
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");   

  } // end of main method
} // end of class